// RingBuffer.cpp : 콘솔 응용 프로그램에 대한 진입점을 정의합니다.
//
#include <stdio.h>
#include <Windows.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <process.h>
#include "RingBuffer.h"
#include "Log.h"

using namespace serverlibrary::ringbuffer;

//#define SLEEP_FLAG
#ifdef SLEEP_FLAG
#define SLEEP_SET Sleep(500)
#else
#define SLEEP_SET 
#endif

const int test_size = 65;
RingBuffer g_ring_buffer(test_size);
char* g_buf;
char g_data_buf[82];
int count_tick = 0;

unsigned int InputData(void* arguments);
unsigned int OutData(void* arguments);
unsigned int ThreadTest();

unsigned int ThreadTest()
{
	HANDLE h_in_thread;
	unsigned int in_thread_ID;

	HANDLE h_out_thread;
	unsigned int out_thread_ID;

	h_in_thread = (HANDLE)_beginthreadex(NULL, 0, &InputData, NULL, 0, &in_thread_ID);
	h_out_thread = (HANDLE)_beginthreadex(NULL, 0, &OutData, NULL, 0, &out_thread_ID);

	int i = 0;
	scanf_s("%d", &i);

	return 0;
}

unsigned int InputData(void* arguments)
{
	srand((unsigned int)time(nullptr));

	char data[] = "1234567890 abcdefghijklmnopqrstuvwxyz 1234567890 abcdefghijklmnopqrstuvwxyz 12345";

	int in_size = rand() % (test_size);
	int count = 0;

	while (1)
	{
		if (count == sizeof data - 1)
		{
			count = 0;
		}

		if (count + in_size >= sizeof data)
		{
			int ret_val = g_ring_buffer.Enqueue(data + count, sizeof data - count - 1);
			count += ret_val;
		}
		else
		{
			int ret_val = g_ring_buffer.Enqueue(data + count, in_size);
			count += ret_val;
		}
	}
}

unsigned int OutData(void* arguments)
{
	srand((unsigned int)time(nullptr));

	char data[] = "1234567890 abcdefghijklmnopqrstuvwxyz 1234567890 abcdefghijklmnopqrstuvwxyz 12345";
	char out_data[test_size];

	memset(out_data, 0, sizeof out_data);

	while (1)
	{
		int out_size = rand() % (test_size);

		g_ring_buffer.Dequeue(out_data, out_size);

		printf("%s", out_data);

		memset(out_data, 0, sizeof out_data);
		
		SLEEP_SET;

	}
}

/* ***************************************
82사이즈 배열의 데이터를 순환하며
임의로 enqueue, dequeue하여 제대로
동작하는지 확인
**************************************** */
void RandomTest1()
{
	char data[] = "1234567890 abcdefghijklmnopqrstuvwxyz 1234567890 abcdefghijklmnopqrstuvwxyz 12345";
	char out_data[test_size];
	char cmp_out_data[test_size];

	memset(out_data, 0, sizeof out_data);
	memset(cmp_out_data, 0, sizeof cmp_out_data);

	g_buf = g_ring_buffer.GetBufferPtr();
	char* p_data = data;
	int count = 0;
	int tick = 0;

	while (1)
	{
		int in_size = rand() % (test_size);
		int out_size = rand() % (test_size);

		/* ***************************************
		출력 확인을 텍스트로 하기 때문에
		널문자를 조심.
		즉, 복사가 12345 널문자 1234567... 되었고
		복사된 내용을 모두 가져왔을 때
		출력하면 12345 까지만 출력됨.
		**************************************** */
		if (count == sizeof data - 1)
		{
			count = 0;
		}

		if (count + in_size >= sizeof data)
		{
			int ret_val = g_ring_buffer.Enqueue(data + count, sizeof data - count - 1);
			count += ret_val;
		}
		else
		{
			int ret_val = g_ring_buffer.Enqueue(data + count, in_size);
			count += ret_val;
		}

		g_ring_buffer.Peek(cmp_out_data, out_size);
		g_ring_buffer.Dequeue(out_data, out_size);

		if (memcmp(out_data, cmp_out_data, test_size) != 0)
		{
			printf("\n Error \n");
			getchar();
		}

		printf("%s", out_data);
		memset(out_data, 0, sizeof out_data);
		memset(cmp_out_data, 0, sizeof cmp_out_data);

		SLEEP_SET;
	}
}

void SpecificTest()
{
	char data[] = "1234567890 abcdefghijklmnopqrstuvwxyz 1234567890 abcdefghijklmnopqrstuvwxyz 12345";
	char out_data[test_size];
	char cmp_out_data[test_size];

	memset(out_data, 0, sizeof out_data);
	memset(cmp_out_data, 0, sizeof cmp_out_data);

	g_buf = g_ring_buffer.GetBufferPtr();
	char* p_data = data;
	int count = 0;
	int tick = 0;

	g_ring_buffer.Enqueue(data, sizeof data);
	printf("%d \n", g_ring_buffer.GetUsedSize());
	printf("%d \n", g_ring_buffer.GetFreeSize());

	g_ring_buffer.Dequeue(out_data, sizeof data);
	printf("%d \n", g_ring_buffer.GetUsedSize());
	printf("%d \n", g_ring_buffer.GetFreeSize());

	g_ring_buffer.Enqueue(data + 11, 1);
	printf("%d \n", g_ring_buffer.GetUsedSize());
	printf("%d \n", g_ring_buffer.GetFreeSize());

	g_ring_buffer.Dequeue(out_data, 5);
	printf("%d \n", g_ring_buffer.GetUsedSize());
	printf("%d \n", g_ring_buffer.GetFreeSize());

	g_ring_buffer.Dequeue(out_data, 5);
	printf("%d \n", g_ring_buffer.GetUsedSize());
	printf("%d \n", g_ring_buffer.GetFreeSize());
}

int ImitateRecv(char* buf, int len)
{
	char data[] = "1234567890 abcdefghijklmnopqrstuvwxyz 1234567890 abcdefghijklmnopqrstuvwxyz 12345";
	static int count = 0;
	if (count == sizeof data - 1)
	{
		count = 0;
	}
	int in_size = rand() % (sizeof data + 1);

	if (len > in_size)
	{
		len = in_size;
	}

	if (count + len >= sizeof data)
	{
		int copy_len = sizeof data - count - 1;
		memcpy_s(buf, len, data + count, copy_len);
		count += copy_len;

		return copy_len;
	}
	else
	{
		memcpy_s(buf, len, data + count, len);
		count += len;

		return len;
	}
}

int ImitateSend(char* buf, int len)
{
	int send_size = rand() % (sizeof g_data_buf + 1);
	if (len > send_size)
	{
		len = send_size;
	}

	memcpy_s(g_data_buf, sizeof g_data_buf, buf, len);

	return len;
}

/* ***************************************
소켓 API중의 recv를 모방하여
int read_count = recv(socket, lingbuffer.GetWriteBufferPtr(), lingbuffer.GetWritableSizeAtOnce());
lingbuffer.MoveRearAfterRead(read_count);
가 제대로 동작하는지 확인
**************************************** */
int SimulateNetwork()
{
	char data[test_size];
	memset(data, 0, sizeof data);

	while (1)
	{
		if (count_tick == 5903)
		{
			printf("");
		}
		int val = g_ring_buffer.GetWritableSizeAtOnce();
		int read_count = ImitateRecv(g_ring_buffer.GetWriteBufferPtr(), g_ring_buffer.GetWritableSizeAtOnce());
		g_ring_buffer.MoveRearAfterWrite(read_count);

		int out_size = rand() % test_size;
		int dequeue_count = g_ring_buffer.Dequeue(data, out_size);

		printf("%s", data);
		memset(data, 0, sizeof data);

		count_tick++;
		SLEEP_SET;
	}
}

/* ***************************************
소켓 API중의 send를 모방하여
int send_count = send(socket, lingbuffer.GetReadBufferPtr(), lingbuffer.GetReadableSizeAtOnce());
lingbuffer.MoveFrontAfterRead(read_count);
가 제대로 동작하는지 확인
**************************************** */
void SimulateNetwork2()
{
	memset(g_data_buf, 0, 82);

	char data[] = "1234567890 abcdefghijklmnopqrstuvwxyz 1234567890 abcdefghijklmnopqrstuvwxyz 12345";
	int count = 0;

	while (1)
	{
		if (count == sizeof data - 1)
		{
			count = 0;
		}

		int in_size = rand() % (sizeof data + 1);

		if (count + in_size >= sizeof data)
		{
			int enqueue_count = g_ring_buffer.Enqueue(data + count, sizeof data - count - 1);
			count += enqueue_count;
		}
		else
		{
			int enqueue_count = g_ring_buffer.Enqueue(data + count, in_size);
			count += enqueue_count;
		}

		int send_count = ImitateSend(g_ring_buffer.GetReadBufferPtr(), g_ring_buffer.GetReadableSizeAtOnce());
		g_ring_buffer.MoveFrontAfterRead(send_count);

		printf("%s", g_data_buf);
		memset(g_data_buf, 0, 82);

		SLEEP_SET;
	}
}

int main()
{
	ThreadTest();


	//RandomTest1();
	//SpecificTest();
	//SimulateNetwork();
	//SimulateNetwork2();

	//RingBuffer buffer{ 7 };

	//wprintf(L" total size : %d \n", buffer.GetBufferSize());
	//wprintf(L" free size : %d \n", buffer.GetFreeSize());
	//
	//char buf[4] = { 1,2,3,4 };
	//buffer.Enqueue(buf, sizeof buf);
	//buffer.Dequeue(buf, sizeof buf);
	//buffer.Enqueue(buf, 3);

	//wprintf(L" free size :%d \n", buffer.GetWritableSizeAtOnce());

	return 0;
}