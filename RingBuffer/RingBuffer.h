#pragma once

/* ***************************************
[RingBuffer] 

Empty:            Full:
ㅁㅁㅁㅁㅁㅁㅁ    ㅁㅁㅁㅁㅁㅁㅁ
 F				     F
 R		           R

Enqueue
ㅇㅁㅁㅁㅁㅁㅁ
 F
   R

Enqueue
ㅇㅇㅁㅁㅁㅁㅁ
 F
     R

Dequeue
ㅁㅇㅁㅁㅁㅁㅁ
   F
     R

GetWirteBufferPtr : 마지막 인덱스를 반환
ㅁㅁㅁㅁㅁㅁㅁ
 F           R

GetReadBufferPtr : 마지막 인덱스를 반환
ㅁㅁㅁㅁㅁㅁㅁ
 R           F

GetWritableSizeAtOnce : 3 반환
ㅁㅁㅁㅁㅁㅁㅁ
 F       R

GetWritableSizeAtOnce : 3 반환
ㅁㅁㅁㅁㅁㅁㅁ
 R     F

GetReadableSizeAtOnce : 3 반환
ㅁㅁㅁㅁㅁㅁㅁ
 R	     F

GetReadableSizeAtOnce : 6 반환
ㅁㅁㅁㅁㅁㅁㅁ
 F			 R

GetUsedSize : 2 반환 
ㅁㅁㅁㅁㅁㅁㅁ
 F   R

GetFreeSize : 6 반환. 
ㅁㅁㅁㅁㅁㅁㅁ
F
R
example) size가 1024, rear,front =0인 경우, GetFreeSize()를 호출하면 1023이 반환. 
why? front와 rear가 full인 경우와 empty인 경우를 GetFreeSize로 조절
**************************************** */

namespace serverlibrary
{
	namespace ringbuffer
	{
		class RingBuffer final
		{
		public:
			enum class eRingBuffer
			{
				DEFAULT_BUFFER_SIZE = 8192,
			};

			RingBuffer(int buffer_size = static_cast<int>(eRingBuffer::DEFAULT_BUFFER_SIZE));
			~RingBuffer();
			RingBuffer(const RingBuffer& copy) = delete;
			RingBuffer(RingBuffer&& other) = delete;

			int GetBufferSize() const;
			int GetUsedSize() const;
			int GetUsedSize(int rear, int front) const;
			int GetFreeSize() const;
			int GetFreeSize(int rear, int front) const;
			int GetWritableSizeAtOnce() const;
			int GetWritableSizeAtOnce(int rear, int front) const;
			int GetReadableSizeAtOnce() const;
			int GetReadableSizeAtOnce(int rear, int front) const;

			char* GetBufferPtr() const;
			char* GetWriteBufferPtr() const;
			char* GetReadBufferPtr() const;

			/* ***************************************
			Enqueue나 Dequeue가 아니라 
			GetBufferPtr과 같은 함수를 통하여 버퍼의 인덱스에 직접 접근했다면
			Move를 호출해야함.
			**************************************** */
			void MoveFrontAfterRead(int target_size);
			void MoveRearAfterWrite(int target_size);

			int Enqueue(const char* const data, int write_size);
			int Dequeue(char* const data, int read_size);
			int Peek(char* const data, int read_size) const;

			void ClearBuffer();

			/* ***************************************
			링버퍼는 멀티 스레드 상황도 염두해두었으므로 Lock과 UnLock을 추가하였음.
			그러나, 한 스레드에서는 Enqueue만 하고 다른 한 스레드에서는 Dequeue만 한다면,
			Lock UnLock은 필요 없음.
			**************************************** */
			void Lock();
			void Unlock();

		private:
			char*	_buffer;
			int		_size;
			int		_front;
			int		_rear;
			CRITICAL_SECTION _cs;
		};
	}
}
