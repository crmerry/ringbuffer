#include <stdio.h>
#include <string>
#include <time.h>
#include <assert.h>
#include "Log.h"

/* ***************************************
로그용 전역 변수
**************************************** */
int		g_log_level;
wchar_t	g_log_temp_buf[1024];
FILE*	g_log_file_fp;
wchar_t g_log_file_name[64];

/* ***************************************
로그용 함수
**************************************** */
bool InitializeLog()
{
	g_log_level = LOG_LEVEL_DEBUG;

	struct tm t;
	time_t current;

	time(&current);
	localtime_s(&t, &current);

	swprintf_s(g_log_file_name, 64, L".\\LOG\\LOG_%d_%d_%d_%d_%d_%d.txt", t.tm_year + 1900, t.tm_mon + 1, t.tm_mday, t.tm_hour, t.tm_min, t.tm_sec);

	int error =_wfopen_s(&g_log_file_fp, g_log_file_name, L"w");

	if (g_log_file_fp == nullptr)
	{
		assert(0);

		return false;
	}

	fclose(g_log_file_fp);

	return true;
}

void ReleaseLog()
{
	
}

void Log(wchar_t* log_string, int log_level)
{
	struct tm t;
	time_t current;

	time(&current);
	localtime_s(&t, &current);

	wchar_t time_string[64];
	swprintf_s(time_string, 64, L"%d_%d_%d_%d_%d_%d.txt", t.tm_year + 1900, t.tm_mon + 1, t.tm_mday, t.tm_hour, t.tm_min, t.tm_sec);

	wchar_t log_level_string[12];

	switch (log_level)
	{
	case LOG_LEVEL_DEBUG:
		wcscpy_s(log_level_string, 12, L"DEBUG");
		break;

	case LOG_LEVEL_WARNING:
		wcscpy_s(log_level_string, 12, L"WARNG");
		break;

	case LOG_LEVEL_ERROR:
		wcscpy_s(log_level_string, 12, L"ERROR");
		break;

	case LOG_LEVEL_SYSTEM:
		wcscpy_s(log_level_string, 12, L"SYSTM");
		break;

	default:
		wcscpy_s(log_level_string, 12, L"OUTRN");
		break;
	}

	int error = _wfopen_s(&g_log_file_fp, g_log_file_name, L"a");
	
	if (g_log_file_fp)
	{
		fwprintf(g_log_file_fp, L"| %ls | %ls | %ls \n", log_level_string, time_string, log_string);
		fclose(g_log_file_fp);
	}
	else
	{
		int a = 10;
		assert(0);
	}
}