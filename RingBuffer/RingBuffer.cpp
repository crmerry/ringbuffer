#include "stdafx.h"
#include <stdlib.h>
#include <Windows.h>
#include "RingBuffer.h"

namespace serverlibrary
{
	namespace ringbuffer
	{
		RingBuffer::RingBuffer(int buffer_size)
			: _size(buffer_size), _front(0), _rear(0)
		{
			_buffer = new char[buffer_size];
			InitializeCriticalSection(&_cs);
		}

		RingBuffer::~RingBuffer()
		{
			DeleteCriticalSection(&_cs);
			delete[] _buffer;
		}

		int RingBuffer::GetBufferSize() const
		{
			return _size;
		}

		int RingBuffer::GetUsedSize() const
		{
			/* ***************************************
			한 링버퍼에 대하여 lock 없이,
			두 개의 쓰레드가 enqueue, dequeue를 하려면
			멤버 변수를 지역으로 복사하여 사용해야 함.
			**************************************** */
			const int rear = _rear;
			const int front = _front;

			if (rear >= front)
			{
				return rear - front;
			}
			else
			{
				return _size - front + rear;
			}
		}

		/* ***************************************
		thread-safe를 위한 GetUsedSIze().
		**************************************** */
		int RingBuffer::GetUsedSize(int rear, int front) const
		{
			if (rear >= front)
			{
				return rear - front;
			}
			else
			{
				return _size - front + rear;
			}
		}

		int RingBuffer::GetFreeSize() const
		{
			const int rear = _rear;
			const int front = _front;

			if (rear >= front)
			{
				return _size - rear + front - 1;
			}
			else
			{
				return front - rear - 1;
			}
		}

		int RingBuffer::GetFreeSize(int rear, int front) const
		{
			if (rear >= front)
			{
				return _size - rear + front - 1;
			}
			else
			{
				return front - rear - 1;
			}
		}

		/* ***************************************
		한 번에, 버퍼에 쓸 수있는 크기
		1) ㅁㅁFㅁㅁㅁRㅁㅁ,
		: R 포함 3칸

		2) ㅁㅁRㅁㅁㅁFㅁㅁ,
		: R 포함 4칸

		3) ㅁㅁFㅁㅁㅁㅁㅁR,
		: R 포함 1칸
		**************************************** */
		int RingBuffer::GetWritableSizeAtOnce() const
		{
			const int rear = _rear;
			const int front = _front;

			if (rear >= front)
			{
				if (front == 0)
				{
					return _size - rear - 1;
				}

				return _size - rear;
			}
			else
			{
				return front - rear - 1;
			}
		}

		int RingBuffer::GetWritableSizeAtOnce(int rear, int front) const
		{
			if (rear >= front)
			{
				if (front == 0)
				{
					return _size - rear - 1;
				}

				return _size - rear;
			}
			else
			{
				return front - rear - 1;
			}
		}

		/* ***************************************
		한 번에 버퍼로부터 읽을 수 있는 크기
		1) ㅁㅁFㅁㅁㅁRㅁㅁ
		: F 포함 4칸, 다 읽으면 F,R 위치 동일

		2) ㅁㅁRㅁㅁㅁFㅁㅁ
		: F 포함 3칸, 다 읽으면 F는 인덱스 0으로 이동됨.

		3) ㅁㅁRㅁㅁㅁㅁㅁF
		: F 포함 1칸
		**************************************** */
		int RingBuffer::GetReadableSizeAtOnce() const
		{
			const int rear = _rear;
			const int front = _front;

			if (rear >= front)
			{
				return rear - front;
			}
			else
			{
				return _size - front;
			}
		}

		int RingBuffer::GetReadableSizeAtOnce(int rear, int front) const
		{
			if (rear >= front)
			{
				return rear - front;
			}
			else
			{
				return _size - front;
			}
		}

		char* RingBuffer::GetBufferPtr() const
		{
			return _buffer;
		}

		/* ***************************************
		1)
		ㅁㅁFㅁㅁRㅁㅁ
		인덱스 5(R)에 대한 포인터를 반환.

		2)
		ㅁㅁFㅁㅁㅁㅁR
		인덱스 7(R)에 대한 포인터를 반환.
		**************************************** */
		char* RingBuffer::GetWriteBufferPtr() const
		{
			return &_buffer[_rear];
		}

		/* ***************************************
		1)
		ㅁㅁFㅁㅁRㅁㅁ
		인덱스 2(F)에 대한 포인터를 반환.

		2)
		ㅁㅁRㅁㅁㅁㅁF
		인덱스 7(F)에 대한 포인터를 반환.
		**************************************** */
		char* RingBuffer::GetReadBufferPtr() const
		{
			return &_buffer[_front];
		}

		/* ***************************************
		외부에서 GetWriteBufferPtr를 통하여 데이터에 접근할 때 사용.
		Enqueue를 호출하지 않는 경우, rear가 움직이지 않는다.
		따라서 rear를 움직이기 위한 호출이 필요.
		**************************************** */
		void RingBuffer::MoveRearAfterWrite(int target_size)
		{
			_rear = (_rear + target_size) % _size;
		}

		/* ***************************************
		외부에서 GetReadBufferPtr을 통하여 데이터에 접근할 때 사용.
		Dequeue를 호출하지 않는 경우, front가 움직이지 않는다.
		따라서 front를 움직이기 위한 호출이 필요.
		**************************************** */
		void RingBuffer::MoveFrontAfterRead(int target_size)
		{
			_front = (_front + target_size) % _size;
		}

		/* ***************************************
		1> in_size와 GetFreeSize()를 비교하여
		반영할 Enqueue사이즈 획득.

		2> GetWritableSizeAtOnce()로 한 번에 Enqueue할 수 있는 양을 구하고
		in_size보다 크면 바로 카피
		in_size보다 작으면 분할해서 카피.
		**************************************** */
		int RingBuffer::Enqueue(const char* const data, int in_size)
		{
			const int rear = _rear;
			const int front = _front;
			const int usable_size = GetFreeSize(rear, front);

			if (in_size > usable_size)
			{
				in_size = usable_size;
			}

			const int writable_size_atonce = GetWritableSizeAtOnce(rear, front);

			if (writable_size_atonce >= in_size)
			{
				memcpy_s(&_buffer[rear], in_size, data, in_size);
				_rear = (rear + in_size) % _size;
			}
			else
			{
				memcpy_s(&_buffer[rear], writable_size_atonce, data, writable_size_atonce);
				const int left_size = in_size - writable_size_atonce;
				memcpy_s(_buffer, left_size, data + writable_size_atonce, left_size);
				_rear = left_size;
			}

			return in_size;
		}

		/* ***************************************
		1> out_size와 GetusedSize()를 비교하여
		반영할 Dequeue사이즈 획득.

		2> GetReadableSizeAtOnce()로 한 번에 Dequeue할 수 있는 양을 구하고
		out_size보다 크면 바로 카피
		out_size보다 작으면 분할해서 카피.
		**************************************** */
		int RingBuffer::Dequeue(char* const data, int out_size)
		{
			const int rear = _rear;
			const int front = _front;
			const int used_size = GetUsedSize(rear, front);

			if (out_size > used_size)
			{
				out_size = used_size;
			}

			const int readable_size_atonce = GetReadableSizeAtOnce(rear, front);

			if (readable_size_atonce >= out_size)
			{
				memcpy_s(data, out_size, &_buffer[front], out_size);
				_front = (front + out_size) % _size;
			}
			else
			{
				memcpy_s(data, readable_size_atonce, &_buffer[front], readable_size_atonce);
				const int left_size = out_size - readable_size_atonce;
				memcpy_s(data + readable_size_atonce, left_size, _buffer, left_size);
				_front = left_size;
			}

			return out_size;
		}

		/* ***************************************
		Dequeue와 같지만, front 이동 없음.
		**************************************** */
		int RingBuffer::Peek(char* const data, int out_size) const
		{
			const int rear = _rear;
			const int front = _front;
			const int used_size = GetUsedSize(rear, front);

			if (out_size > used_size)
			{
				out_size = used_size;
			}

			const int once_write_size = GetReadableSizeAtOnce(rear, front);

			if (once_write_size >= out_size)
			{
				memcpy_s(data, out_size, &_buffer[front], out_size);
			}
			else
			{
				memcpy_s(data, once_write_size, &_buffer[front], once_write_size);
				int left_size = out_size - once_write_size;
				memcpy_s(data + once_write_size, left_size, _buffer, left_size);
			}

			return out_size;
		}

		/* ***************************************
		링버퍼는 front와 rear의 위치를 바꿔가며 사용하는 것, 따라서 내용을 지울 때는
		특별히 초기화할 필요 없이 인덱스만 바꿔주면된다. 마치 스택 메모리처럼.
		**************************************** */
		void RingBuffer::ClearBuffer()
		{
			_front = 0;
			_rear = 0;
		}

		void RingBuffer::Lock()
		{
			EnterCriticalSection(&_cs);
		}

		void RingBuffer::Unlock()
		{
			LeaveCriticalSection(&_cs);
		}
	}
}
